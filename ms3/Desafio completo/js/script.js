// Seleciona o elemento footer do HTML
const footer = document.getElementById('footer');

// Define a cor azul para o texto do footer
footer.style.color = 'blue';

/**
 * função que formata o tempo atual como uma string no formato HH:MM:SS
 * @returns {string} String formatada do tempo atual
 */
function formatTime() {
  const now = new Date();
  const hours = now.getHours().toString().padStart(2, '0');
  const minutes = now.getMinutes().toString().padStart(2, '0');
  const seconds = now.getSeconds().toString().padStart(2, '0');
  return `${hours}:${minutes}:${seconds}`;
}

/**
 * Função que atualiza o texto do footer com o tempo atual formatado
 */
function updateTime() {
  const timeString = formatTime();
  footer.innerHTML = `© Mario Elias ${timeString}`;
}

// chama a função updateTime a cada segundo para atualizar o footer com o tempo atualizado
setInterval(updateTime, 1000);
